package com.example.progmob2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.R;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class RecycleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;
        //Data
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate Data Mahasiswa
        Mahasiswa m1 = new Mahasiswa ("Mael ", "721820332", "098765678987");
        Mahasiswa m2 = new Mahasiswa ("Nael ", "721820335", "098765672345");
        Mahasiswa m3 = new Mahasiswa ("Sael ", "721820334", "09876562317");
        Mahasiswa m4 = new Mahasiswa ("Cael ", "721820333", "0987656712347");
        Mahasiswa m5 = new Mahasiswa ("Vael ", "721820331", "09876232487");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecycleActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecycleActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}