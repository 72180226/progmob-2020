package com.example.progmob2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class TrackerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);
        TextView txtTracker = (TextView) findViewById(R.id.textViewT);

        Bundle v = getIntent().getExtras();
        String textTracker = v.getString("Get_Tracker");
        txtTracker.setText(textTracker);
    }
}